<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventDate extends Model
{
    protected $primaryKey = "Event_Event_id";
    protected $table = "event_date";

    public $timestamps = false;

    public function event(){

        return $this->hasOne('App\Event', 'Event_id', 'Event_Event_id' );

    }

}
