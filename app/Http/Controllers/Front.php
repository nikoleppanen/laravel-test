<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Event;
use App\EventDate;
use App\Location;

use Illuminate\Support\Facades\DB;

class Front extends Controller
{
    var $eventDates;
    var $events;
    var $locations;

    public function __construct() {
        $this->eventDates = EventDate::all(array('Date')); // EventDate on malli-luokan nimi
        $this->events = Event::all(array('Name'));
        $this->locations = Location::all(array('Location_name','Street_address','City'));
    }
    public function index() {
        return view('page', array('events' => $this->events) );
    }
}
