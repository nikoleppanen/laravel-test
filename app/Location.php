<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $primaryKey = "Location_id";
    protected $table = "location";

    public $timestamps = false;

    public function events()
    {
        return $this->hasMany('App\Event', "Location_location_id", 'Location_id' );
    }


}
