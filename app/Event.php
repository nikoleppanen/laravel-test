<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $primaryKey = "Event_id";
    protected $table = "event";

    public $timestamps = false;

    public function location()
    {
        return $this->hasOne('App/Location', 'Location_id', 'Event_id');
    }
}
